# maodv-ccaodv

maodv-ccaodv

HUDA FAUZAN MURTADHO
05111540000022

Congestion Control AODV

Modifikasi ini diberikan ketika Delivery Packet pada Node terjadi congestion sehingga modifikasi yang diberikan dengan memberikan counter/threshold dengan nilai tertentu sebagai patokan apakah sudah terjadi congestion atau tidak dan ketika sudah menemui congestion tersebut packet akan didrop dan mencari route lain untuk mencapai node destination.

Perubahan yang dilakukan pada 
1. menambahkan variabel baru bernama congestion flag didalam aodv_packet.h pada bagian hdr_aodv_reply dengan nilai true/false
2. menambahkan congestion counter pada tabel routing didalam aodv_rtable.cc dan aodv_rtable.h
3. membuat fungsi untuk mengecek jumlah counter apakah sesuai dengan thresholdnya pada aodv.cc class recvRequest. 

